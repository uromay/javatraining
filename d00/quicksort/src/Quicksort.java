/**
 * Created by bpop on 22.02.2016.
 */

public class Quicksort {
    public static void quickSort(String[] array, int start, int end){
        int i = start;
        int j = end;
        if (array.length >= 2){
            String pivot = array[(i + j) / 2];
            while (i <= j){
                while (array[i].compareTo(pivot) < 0 && i < end)
                    i++;
                while (array[j].compareTo(pivot) > 0 && j > start)
                    j--;
                if (i <= j) {
                    String tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                    i++;
                    j--;
                }
            }
            if (start < j)
                quickSort(array, start, j);
            if (i < end)
                quickSort(array, i, end);
        }
    }
    public static void main(String[] args) {
        String myArr[] = {"Hello", "Kitty", "Mark", "Hello", "mark"};
        int i = 0;
        System.out.println("Unsorted:");
        while (i < myArr.length){
            System.out.println(myArr[i]);
            i++;
        }
        quickSort(myArr, 0, myArr.length - 1);
        System.out.println("");
        System.out.println("Sorted:");
        i = 0;
        while (i < myArr.length){
            System.out.println(myArr[i]);
            i++;
        }
    }
}