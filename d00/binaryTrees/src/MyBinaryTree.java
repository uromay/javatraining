/**
 * Created by bpop on 22.02.2016.
 */
public class MyBinaryTree {
    private Node root;
    public MyBinaryTree() {
        this.root = null;
    }
    public void addNode(int key) {
        Node newNode = new Node(key);
        if (root == null){
            root = newNode;
        } else {
            Node current = root;
            Node parent = null;
            while (true) {
                parent = current;
                if (key < current.key) {
                    current = current.left;
                    if (current == null) {
                        parent.left = newNode;
                        return;
                    }
                } else {
                    current = current.right;
                    if (current == null) {
                        parent.right = newNode;
                        return;
                    }
                }
            }
        }
    }
    public boolean deleteNode(int key) {
        Node current = root;
        Node parent = root;
        boolean hasLeft = false;
        while (current.key != key) {
            parent = current;
            if (key < current.key) {
                current = current.left;
                hasLeft = true;
            } else {
                current = current.right;
                hasLeft = false;
            }
            if (current == null)
                return false;
        }
        if (current.right == null && current.left == null) {
            if (current == root)
                current = null;
            if (hasLeft) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        } else if (current.right == null) {
            if (current == root) {
                root = current.left;
            } else if (hasLeft) {
                parent.left = current.left;
            } else {
                parent.right = current.left;
            }
        } else if (current.left == null) {
            if (current == root) {
                root = current.right;
            } else if (hasLeft) {
                parent.left = current.right;
            } else {
                parent.right = current.right;
            }
        } else if (current.right != null && current.left != null) {
            Node replacer = null;
            Node replacerParent = null;
            Node rightCurrent = current.right;
            while (rightCurrent != null) {
                replacerParent = replacer;
                replacer = rightCurrent;
                rightCurrent = rightCurrent.left;
            }
            if (replacer != current.right) {
                replacerParent.left = replacer.right;
                replacer.right = current.right;
            }
            if (current == root) {
                root = replacer;
            } else if (hasLeft) {
                parent.left = replacer;
            } else {
                parent.right = replacer;
            }
            replacer.left = current.left;
        }
        return true;
    }
    public void traverseTree(Node current) {
        if (current != null) {
            traverseTree(current.left);
            System.out.println(current.key);
            traverseTree(current.right);
        }
    }
    public static void main(String[] args) {
        MyBinaryTree bt = new MyBinaryTree();
        bt.addNode(5);
        bt.addNode(8);
        bt.addNode(3);
        bt.addNode(54);
        bt.addNode(15);
        bt.addNode(25);
        bt.addNode(11);
        bt.traverseTree(bt.root);
        bt.deleteNode(15);
        bt.traverseTree(bt.root);
    }
}
