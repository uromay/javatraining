package com.bogdanpop;

/**
 * Created by bpop on 23.02.2016.
 */
public class Artefacts {
//    protected String name;
}
class Weapon extends Artefacts {
    protected int damage;
}
class Defence extends Artefacts {
    protected int health;
}
class Armor extends Defence {
    public Armor() {
        health = 20;
    }
}
class Helm extends Defence {
    public Helm() {
        health = 10;
    }
}
class Sword extends Weapon {
    public Sword() {
        damage = 12;
    }
}
class Staff extends Weapon {
    public Staff() {
        damage = 3;
    }
}
class Bow extends Weapon {
    public Bow() {
        damage = 8;
    }
}