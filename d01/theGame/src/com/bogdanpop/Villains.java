package com.bogdanpop;

/**
 * Created by bpop on 23.02.2016.
 */
public class Villains extends Character {

}
class Devil extends Villains {
    public Devil (String name) {
        initDevil(name);
    }
    public Devil (String name, int level) {
        this.level = level;
        initDevil(name);
    }
    private void initDevil(String name) {
        this.name = name;
        health = 150 + level * 20;
        damage = 30 + level * 5;
    }
}
class Goblin extends Villains {
    public Goblin (String name) {
        initGoblin(name);
    }
    public Goblin (String name, int level) {
        this.level = level;
        initGoblin(name);
    }
    private void initGoblin(String name) {
        this.name = name;
        health = 100 + level * 20;
        damage = 35 + level * 5;
    }
}
class Necromancer extends Villains {
    public Necromancer (String name) {
        initNecromancer(name);
    }
    public Necromancer (String name, int level) {
        this.level = level;
        initNecromancer(name);
    }
    private void initNecromancer(String name) {
        this.name = name;
        health = 130 + level * 20;
        damage = 21 + level * 5;
    }
}
class DarkMage extends Villains {
    public DarkMage (String name) {
        initDarkMage(name);
    }
    public DarkMage (String name, int level) {
        this.level = level;
        initDarkMage(name);
    }
    private void initDarkMage(String name) {
        this.name = name;
        health = 180 + level * 20;
        damage = 28 + level * 5;
    }
}