package com.bogdanpop;

/**
 * Created by bpop on 23.02.2016.
 */
public class Character {
    protected int level;
    protected int health;
    protected int damage;
    protected int id;
    private static int iter;
    protected String name;
    public Character () {
        id = ++iter;
        level = 1;
    }
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object instanceof Character && this.id == ((Character)object).id) {
            return true;
        }
        return false;
    }
    protected void getDamage(int damage) {
        health -= damage;
    }
    public void addArtefact (String artefact) {
        if (artefact.equalsIgnoreCase("Armor")) {
            Armor armor = new Armor();
            health += armor.health;
        }
        if (artefact.equalsIgnoreCase("Helm")) {
            Helm helm = new Helm();
            health += helm.health;
        }
        if (artefact.equalsIgnoreCase("Sword")) {
            Sword sword = new Sword();
            damage += sword.damage;
        }
        if (artefact.equalsIgnoreCase("Staff")) {
            Staff staff = new Staff();
            damage += staff.damage;
        }
        if (artefact.equalsIgnoreCase("Bow")) {
            Bow bow = new Bow();
            damage += bow.damage;
        }
    }
}
