package com.bogdanpop;

/**
 * Created by bpop on 23.02.2016.
 */
public class Main {
    public static void main(String[] args) {
        GameGui gameGui = new GameGui();
        Character hero = new Elf("Ghita", 7);
        Character villain = new DarkMage("Costel", 3);
        hero.addArtefact("Helmet");
        hero.addArtefact("Staff");
        villain.addArtefact("Armor");
        villain.addArtefact("Sword");

        if (hero.equals(villain)) {
            return;
        } else {
            System.out.println("Battle begins.");
        }
        while (hero.health > 0 && villain.health > 0) {
            villain.getDamage(hero.damage);
            if (villain.health <= 0) {
                System.out.println("Hero " + hero.name + " id(" + hero.id + ") has dealt " + hero.damage + " damage and slain the villain " + villain.name + ".");
                break;
            }
            System.out.println("Hero " + hero.name + " id(" + hero.id + ") dealt " + hero.damage + " damage to villain " + villain.name + ". He has " + villain.health + " health left.");
            hero.getDamage(villain.damage);
            if (hero.health <= 0) {
                System.out.println("Villain " + villain.name + " id(" + villain.id + ") has dealt " + villain.damage + " damage and slain the hero " + hero.name + ".");
                break;
            }
            System.out.println("Villain " + villain.name + " id(" + villain.id + ") dealt " + villain.damage + " damage to hero " + hero.name + ". He has " + hero.health + " health left.");
        }
    }
}