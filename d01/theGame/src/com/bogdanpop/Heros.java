package com.bogdanpop;

/**
 * Created by bpop on 23.02.2016.
 */
public class Heros extends Character {
}
class Orc extends Heros {
    public Orc (String name) {
        initOrc(name);
    }
    public Orc (String name, int level) {
        this.level = level;
        initOrc(name);
    }
    private void initOrc(String name) {
        this.name = name;
        health = 200 + level * 20;
        damage = 25 + level * 5;
    }
}
class Elf extends Heros {
    public Elf (String name) {
        initElf(name);
    }
    public Elf (String name, int level) {
        this.level = level;
        initElf(name);
    }
    private void initElf(String name) {
        this.name = name;
        health = 100 + level * 20;
        damage = 30 + level * 5;
    }
}
class Knight extends Heros {
    public Knight (String name) {
        initKnight(name);
    }
    public Knight (String name, int level) {
        this.level = level;
        initKnight(name);
    }
    private void initKnight(String name) {
        this.name = name;
        health = 180 + level * 20;
        damage = 28 + level * 5;
    }
}
class Mage extends Heros {
    public Mage (String name) {
        initMage(name);
    }
    public Mage (String name, int level) {
        this.level = level;
        initMage(name);
    }
    private void initMage(String name) {
        this.name = name;
        health = 140 + level * 20;
        damage = 32 + level * 5;
    }
}